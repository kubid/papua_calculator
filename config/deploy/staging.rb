role :app, %w{wwf@139.0.31.197}
role :web, %w{wwf@139.0.31.197}
role :db,  %w{wwf@139.0.31.197}


server '139.0.31.197', user: 'wwf', roles: %w{web app}, my_property: :my_value
