window.obelusChart = function() {
    window.activechart = [];
		
		function checkBaseline(myArray, num) {
			myLength = myArray.length
			if (myArray.slice(myLength - 1, myLength) == "Baseline" || myArray.slice(myLength - 1, myLength) =="Rasio pasokan bioenergi") {
	    	return num=-1;}
			else {
	    	return num=myLength;}
    };

		
    this.combination = function(chart_name, chart_title, data_x, elem_id) {
        var dup_data = data_x.slice()
        var categories = dup_data[0].slice()
        var categories = categories.splice(1)

        var data_columns = this.filterData(dup_data);
        activechart.push(chart_name)
        var ochart = window[chart_name.replace(/./g, '_')] = c3.generate({
            bindto: elem_id,
            data: {
                columns: data_columns,
                type: 'bar',
                types: {
                    "Tingkat ketersediaan": 'spline'
                },
                axes: {
                    "Tingkat ketersediaan": 'y2'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: categories
                },
                y: {
                    label: 'Milyar Kalori'
                },
                y2: {
                    label: "Persen",
                    show: true
                }
            },
            zoom: {
                enabled: true
            },
            tooltip: {
                format: {
                    title: function(d) {
                        return 'Tahun ' + categories[d];
                    },
                    value: function(value, ratio, id) {
                        var format = id === 'Tingkat ketersediaan' ? d3.format('%')(value / 100) : d3.round(value, 2);
                        return format
                    }
                    //            value: d3.format(',') // apply this format to both y and y2
                }
            },
            color: {
                // pattern: ['#9F3100', '#E56D38', '#F1CD78', '#DEDB95']
            },
            grid: {
                x: {
                    show: false
                },
                y: {
                    show: true
                }
            },
            legend: {
                show: false
            }
            
        });
        d3.select(elem_id).insert("div", ":first-child").attr('class', 'ob-chart-title')
        $(elem_id + ' .ob-chart-title').text(chart_title)
        groups = []
        $.each(dup_data, function(i, e) {
            groups.push(e[0])
        })
        this.customLegend(elem_id, groups, ochart)
    }

    this.stacked = function(chart_name, chart_title, chart_data, elem_id, chart_type, label_y, max_axis) {
        setup = this.setup(chart_data, chart_type)
        setup.data_columns.splice(setup.data_columns.length, 1)
        
        var lineBaseline = checkBaseline(setup.data_groups, lineBaseline) 
        
        // setup.chart_type.Total = "spline"
        y_label_opt = {
            label: label_y,
            position: 'outer-middle',
            padding: {bottom:0},
            tick: {
                format: function(y) {
                  if (y <= 0.99 && y !== 0.0 && y >= -0.99) {
                    return d3.format('.2f')(y);
                  } else {
                    return d3.format(',')(y);
                  }
                }
            }
        }

        if (max_axis !== undefined) {
            y_label_opt.max = max_axis
        }
        
        if (y_label_opt.label ==  "%") {
        	y_label_opt.min = 0;
        	y_label_opt.max = 100;
        	y_label_opt.tick.values = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
        }

        setup.data_groups.pop()


        activechart.push(chart_name)
        var ochart = window[chart_name.replace(/\./g, '_')] = c3.generate({
            bindto: elem_id,
            data: {
                columns: setup.data_columns,
                type: chart_type,
                types: {
                    'Total': 'line',
                    'Total lahan': 'line',
                    'Total pembangkitan': 'line',
                    'Total kapasitas': 'line',
                    'Baseline': 'line',
                },
                groups: [setup.data_groups.slice(0,lineBaseline)],
                order: 'asc',
                colors: this.chartColors()
            },
            axes: {
                "Total": 'y2'
            },
            axis: {
                x: {
                    type: 'category',
                    categories: setup.categories,
                    tick: {
										rotate: 90,
										multiline: false
		    						},
                },
                y: y_label_opt
            },
            color: {
                // pattern: ['#FF001E', '#FF6200', '#FF8000', '#FFB300', '#FFD000']
            },
            point: { r : 3},
            tooltip: {
                grouped: true, // default true
                format: {
                    title: function(d) {
                        return 'Tahun ' + categories[d];
                    },
                    value: function(value, ratio, id) {

                        if (label_y == "%") {
                            return d3.format('%')(value / 100)
                        } else {
                            return d3.format(',')(value.toFixed(2)) + " " + label_y
                        }
                    }
                    //            value: d3.format(',') // apply this format to both y and y2
                }
            },

            grid: {
                x: {
                    show: false
                },
                y: {
                    show: true
                }
            },
            zoom: {
                enabled: true
            },
            point: {
                show: chart_title == "Ketergantungan Impor" ? true : false
            },
            legend: {
                show: false
            }


        });
        d3.select(elem_id).insert("div", ":first-child").attr('class', 'ob-chart-title')
        $(elem_id + ' .ob-chart-title').text(chart_title)
        this.customLegend(elem_id, setup.groups, ochart)
    }

    this.updateStacked = function(chart_name, chart_title, chart_data, chart_type, max_axis) {
        setup = this.setup(chart_data, chart_type)
        setup.data_columns.splice(setup.data_columns.length, 1)
        dchart = window[chart_name.replace(/\./g, '_')]
        dchart.load({
            columns: setup.data_columns
        })
        console.log('-----------------------------0:' + max_axis)
        if (max_axis != null) {
            console.log('-----------------------------1:' + max_axis)
            dchart.axis.max(max_axis)
        }
        activechart.push(chart_name)
    }

    //  custom legend
    this.customLegend = function(elem_id, groups, chart) {
        d3.select(elem_id).insert('div', '.chart').attr('class', 'ob-chart-legend').selectAll('span')
            .data(groups)
            .enter().append('div')
            .attr('data-id', function(id) {
                return id;
            })
            .html(function(id) {
                return '<span class="block" style="background-color:'+ chart.color(id) +'"></span><span class="text-legend">'+id+'</span>'
            })
            .each(function(id) {
                // new_text = this.textContent.replace("Pembangkit listrik ", "")
                // text = this.textContent
                d3.select(this).attr('class', 'ob-legend-text-container')                
                d3.select(this).append('div').attr('class', 'clearfix')
            })
            .on('mouseover', function(id) {
                chart.focus(id);
            })
            .on('mouseout', function(id) {
                chart.revert();
            })
            .on('click', function(id) {
                $('[data-id="'+id+'"]').toggleClass('disabled-legend')
                chart.toggle(id);
            });

        lHeight = $(elem_id).children('.ob-chart-legend').height() + 60
        $(elem_id).css('margin-bottom', lHeight + 'px')
    }

    //  setup parameters
    this.setup = function(data_ori, chart_type) {
        dup_data = data_ori.slice()
        last_elem = dup_data[dup_data.length - 1][0]
        // if (last_elem.toLocaleLowerCase().indexOf("total") == 0) {
        //     dup_data.splice(dup_data.length - 1, 1)
        // }

        cat = dup_data.splice(0, 1)[0]
        types = {}, groups = [], data_groups = []
        categories = cat.slice()
        categories.splice(0, 1)

        $.each(dup_data, function(i, e) {
            types[e[0]] = chart_type
        })
        $.each(dup_data, function(i, e) {
            groups.push(e[0])
            if (chart_type != 'line') {
                data_groups.push(e[0])
            }
        })
        return {
            data_columns: dup_data,
            categories: categories,
            chart_type: types,
            groups: groups,
            data_groups: data_groups
        }
    }

    this.chartColors = function() {
        colors = {
        	//PRIMARY ENERGI
        		'Batubara': '#000000',
						'Produksi batubara': '#000000',
						'Impor batubara': '#000000',
						'Minyak bumi': '#111E06',
						'Gas bumi': '#3e96cc',
						'Hidro': '#00eeee',
						'Surya': '#e5e500',
						'EBT lainnya': '#66cd00',
					// SECONDARY FUELS
						'Bio-energi': '#008000',
						'Biomassa': '#008000',
						'Diesel':  '#69827f',
					//PEMBANGKITAN LISTRIK
						'PLTU': '#000000',
						'PLTMG': '#3e96cc',
						'PLTD': '#49586c',
						'PLTA/PLTMH': '#00eeee',
						'PLTS': '#e5e500',
						'PLTBiomassa': '#008000',
						
					//DEMAND SECTORS
						'Rumah tangga': '#ae5a41',
						'Komersial': '	#1b85b8',
						'Transportasi': '#5a5255',
						'Industri': '#559e83',
						'Pertanian, kontruksi dan pertambangan': '#c3cb71',
					//LAND-USE
            'Perkebunan non-kelapa sawit': '#BCBD22',
            'Hutan yang tersisa':   '#2CA02C',        
            'Lahan kritis':         '#D62728',
            'Bioenergi non-sawit':  '#8C564B',
            "Penggunaan lahan lainnya": '#1F77B4',
            "Perkebunan kelapa sawit": '#FC7458',
            "Bioenergi - sawit": '#FF7F0E',
            "Pertanian": '#E377C2',
            "Kehutanan-HTI": '#406F30',
            "Kehutanan-HPH": '#ADBD80',
          //TOTAL
						"Total": '#740001',
						"Total pembangkitan": '#740001',
						"Total kapasitas": '#740001',
						"Baseline": 'red'
        }
        return colors
    }
    //  filter_data
    this.filterData = function(data) {
        data.splice(0, 1);

        $.each(data, function(i, e) {
            if (i == 2) {
                data.push([])
                $.each(data[i], function(idx, x) {
                    if (idx > 0) {
                        x = x * 100
                    }
                    data[i + 1].push(x);
                })
            }
        })
        data.splice(2, 1);
        return data
    }
}
