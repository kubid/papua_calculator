window.twentyfifty.views.security = function() {
  
      this.setup = function() {
        this.ready = true;
        $('#ob-chart-container, #ob-mini-paper').hide()  
        $('#classic_controls').show()      
        return $('#results').append("<div id='energysecurity'><div id='energy-demand' class='column'></div><div id='energy-supply' class='column'></div><div id='electricity-supply' class='column'></div><div class='clear'></div></div>");
      };

    this.teardown = function() {
      this.ready = false;
      return $('#results').empty();
    };
    
   function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
   };

    this.updateResults = function(pathway) {
      this.pathway = pathway;
      if (!this.ready) {
        this.setup();
      }
      this.updatedElectricitySupply();
      this.updatedEnergySupply();
      this.updatedEnergyDemand();
    };

    this.updatedElectricitySupply = function() {
      var element, _ref3, genshare_type, genshare_value;
      _ref3 = this.pathway.energy_security.generationshare;
      genshare_type = _ref3[0];
      genshare_value = Number(_ref3[1]) * 100;
      element = $('#electricity-supply');
      element.empty();
      element.append("<h2>Pasokan Listrik</h2>");
      element.append("<p>Permintaan listrik yang ada akan dipenuhi oleh sejumlah pembangkit listrik. Pembangkit listrik  tersebut dapat berupa pembangkit berbahan bakar fosil atau energi baru dan terbarukan (EBT). Semakin besar pangsa pembangkit EBT, akan semakin rendah emisi yang dihasilkan dari sektor permbangkitan listrik. Pada skenario ini, pangsa pembangkit <i>" + genshare_type + "</i> memiliki pangsa terbesar, atau " + genshare_value.toFixed(2) + "% dari total kapasitas pembangkit yang ada.</p>");
      element.append("<table class='electricity-supply ob-table-2'>");
      elem = $('table.electricity-supply')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>GWh/yr</th><th class='value'>GWh/yr</th></tr>");
      _ref = this.pathway.electricity.supply;
      idx_2011 = 1, idx_2050 = 9;
      
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        // if(name != "0" && values[0] != "Total"){
        if(name != "0" ){
          if(values[idx_2011] == null){
            var var_2011 = 0
          }else{
            var var_2011 = formatNumber(values[idx_2011].toFixed(2))
          }
          if(values[idx_2050] == null){
            var var_2050 = 0
          }else{
            var var_2050 = formatNumber(values[idx_2050].toFixed(2))
          }
          
          if(values[0]=="Total pembangkitan"){
          	elem.append("<tr><td class='description'><b>" + values[0] + "</b></td><td class='value'><b>" + var_2011 + "</b></td><td class='value'><b>" + var_2050 + "</b></td></tr>");
          	} else {
          	elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + var_2011 + "</td><td class='value'>" + var_2050 + "</td></tr>");
          	}  
        }
        
      }
    };

    this.updatedEnergySupply = function() {
      var element, name, values, _ref, _ref2, _ref3, supply1, supply2;
      _ref = this.pathway.energy_security.supply;
			_ref2 = this.pathway.energy_security.maxprimarysupply;
			_ref3 = this.pathway.energy_security.primarysource;
			
			supply1 = _ref2[0];
			supply2 = _ref2[1];
			supply3 = _ref3[1];
      element = $('#energy-supply');
      element.empty();
      element.append("<h2>Pasokan Energi</h2>");
      element.append("<p>Pasokan <i>" + supply1 + "</i> dan <i>" + supply2 + "</i> akan mendominasi produksi energi hingga tahun 2050. Sumber utama " 
       + supply1 +  " dalam skenario ini berasal dari <i>" + supply3 + "</i>.</p>");
      element.append("<table class='energy-supply ob-table-2'>");
      elem = $('table.energy-supply')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>GWh/yr</th><th class='value'>GWh/yr</th></tr>");
      //_ref = this.pathway.energy_security.supply;
            
      idx_2011 = 1, idx_2050 = 9;
      //console.log('-------My Ref-----:' + _myref)
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        if(name != "0"){
          if (values[0] != 'Total'){
          	elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + formatNumber(values[idx_2011].toFixed(2))+ "</td><td class='value'>" + formatNumber(values[idx_2050].toFixed(2))+ "</td></tr>");
          	} else {
          	elem.append("<tr><td class='description'><b>" + values[0] + "</b></td><td class='value'><b>" + formatNumber(values[idx_2011].toFixed(2))+ "</b></td><td class='value'><b>" + formatNumber(values[idx_2050].toFixed(2))+ "</b></td></tr>");
          }  
        }
        
      }
    };

    this.updatedEnergyDemand = function() {
      var element, name, values, _ref, _myref, _myref2;
      var fuel_1, fuel_2, sector_1, sector_2;
      _myref = this.pathway.energy_security.maxfinaldemand;
      _myref2 = this.pathway.energy_security.maxdemandsector;
      fuel_1 = _myref[0];
      fuel_2 = _myref[1];
      sector_1 = _myref2[0];
      sector_2 = _myref2[1];
      console.log('----My REF---:' + sector_1)
      element = $('#energy-demand');
      element.empty();
      element.append("<h2>Permintaan Energi</h2>");
      element.append("<p>Permintaan energi dalam bentuk <i>" + fuel_1 + "</i> dan <i>" + fuel_2 +  "</i> memdominasi permintaan pada tahun 2050. Sektor <i>" + sector_1 + "</i> akan menjadi penggerak utama permintaan energi " + fuel_1 + ", sedangkan sektor <i>" + sector_2 + "</i> menjadi penggerak permintaan energi " + fuel_2 + ". Keterbatasan pasokan domestik akan menyebabkan peningkatan impor jenis hidrokarbon tersebut, sebagaimana terlihat pada diagram Alur Energi.</p>");
      element.append("<table class='energy-demand ob-table-2'>");
      elem = $('table.energy-demand')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>GWh/yr</th><th class='value'>GWh/yr</th></tr>");
      _ref = this.pathway.energy_security.demand;
      
      idx_2011 = 1, idx_2050 = 9;
      
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        if(name != "0"){
          if (values[0] != 'Total'){
          	elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + formatNumber(values[idx_2011].toFixed(2))+ "</td><td class='value'>" + formatNumber(values[idx_2050].toFixed(2))+ "</td></tr>");
          	} else {
          	elem.append("<tr><td class='description'><b>" + values[0] + "</b></td><td class='value'><b>" + formatNumber(values[idx_2011].toFixed(2))+ "</b></td><td class='value'><b>" + formatNumber(values[idx_2050].toFixed(2))+ "</b></td></tr>");
          }
        }
        
      }
      return element.append("</table>");
    };

  return this;
}.call({});
