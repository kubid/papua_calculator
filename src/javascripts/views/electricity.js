// obelus-c3-chart
window.twentyfifty.views.electricity = function() {

  this.setup    = function(){
    $('#ob-chart-container, #classic_controls').show()
    $('#ob-mini-paper, #energysecurity').hide()
  }
  this.teardown = function(){}

  this.updateResults = function(pathway){
    demand_ary  = pathway.electricity.demand;
    supply_ary  = pathway.electricity.supply;
    capacity_ary     = pathway.electricity.capacity;

    $('.ob-chart').show();

    max1 = obelusMaxValueOfArrayNumber(demand_ary)
    max2 = obelusMaxValueOfArrayNumber(supply_ary)
    max3 = obelusMaxValueOfArrayNumber(capacity_ary)
    max_val = Math.max(max1, max2)

    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('electricity.demand', 'Permintaan Listrik', demand_ary, 'area', max_val)
      chart.updateStacked('electricity.supply', 'Pasokan Listrik',    supply_ary, 'area', max_val)
      chart.updateStacked('electricity.capacity', 'Kapasitas Pembangkit',capacity_ary, 'bar', max3)
    }else{
      chart = new obelusChart();
      chart.stacked('electricity.demand', 'Permintaan Listrik', demand_ary, '#ob-chart1', 'area', 'TWh/yr', max_val)
      chart.stacked('electricity.supply', 'Pasokan Listrik',    supply_ary, '#ob-chart2', 'area', 'TWh/yr', max_val)
      chart.stacked('electricity.capacity', 'Kapasitas Pembangkit',capacity_ary,'#ob-chart3', 'bar', 'MW', max3)
    }
  }
  
  return this;
}.call({});
